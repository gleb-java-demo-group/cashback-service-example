package org.example;

import org.example.service.CashbackCalculationService;

public class Main {
  public static void main(String[] args) {
    CashbackCalculationService service = new CashbackCalculationService();
    int cashback = service.calculate(568_32, 100_00);
    int maxCashback = 3000;
    int cashbackSum = 0;
    int [] cashbackInstances = {12, 42, 543, 2, 154, 768, cashback};
    for (int cashbackInstance : cashbackInstances) {
      cashbackSum = cashbackSum + cashbackInstance;
    }
    if (cashbackSum > maxCashback) {
      cashbackSum = maxCashback;
    }
    System.out.println(cashbackSum);
  }
}
